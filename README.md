# MySQL to Bash

Create directory structure and files from your MySQL database.  Useful for exploring your database using command line tools, e.g. grepping for column names, diffing tables, editing in your favorite text editor.

## How to use

Create a root directory and run the script `mysql-to-bash.sh` from that directory.

```
mkdir MyBashSQL
cd MyBashSQL
source ~/mysql-to-bash/mysql-to-bash.sh
```

This will prompt for your password:

```
Enter password:
```

When the script is finished each Schema will have its own directory and each table will have its own sql file within that directory.

### Prerequisites

mysql, mysqldump

### Installing

Clone the repository and edit the config file to match your database settings.
The script will look for your login and database information under `mysql-to-bash/local_config.sh`, this file is in `.gitignore` so different machines can have different configurations.


```
cp config.sh local_config.sh
```

Open `local_config.sh` and add your host, user, and schemas to the list

```
host=localhost
user=root
schema=( "list" "schemas" "here" )
```

## Acknowledgments

* [PurpleBooth README template](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2)
* [Trutane on StackOverflow](https://stackoverflow.com/a/17016410/9404239)
* [AJ on StackOverflow](https://stackoverflow.com/a/2148876/9404239)
