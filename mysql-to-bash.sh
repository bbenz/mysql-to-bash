#!/bin/bash
my_dir="$(dirname "$0")"
source "$my_dir/local_config.sh"

echo -n "Enter password: "
read -s passwd
for s in "${schema[@]}"
do
  test -d $s || mkdir -p $s
  echo $s
  for t in $(mysql -BN -h $host -u $user -p$passwd -D $s -e 'show tables')
  do
    echo "$s.$t"
    mysqldump --compact --no-data --no-set-names -h $host -u $user -p$passwd $s $t | egrep -v "(^SET|^/\*\!)" > $s/$t.sql
  done
done
